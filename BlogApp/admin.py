from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import Post, Category

# Register your models here.
class PostAdmin(SummernoteModelAdmin):
    list_display = ('title', 'category', 'slug', 'status', 'created_on')
    list_filter = ("status", "category")
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',)}
    summernote_fields = ('content',)

class CategoryAdmin(SummernoteModelAdmin):
    list_display = ('name', 'slug')
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
