from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView
from django.core.paginator import Paginator
from .models import Post

# Create your views here.
class Index(ListView):
    model = Post
    paginate_by = 10
    def get(self, request):
        post_list = Post.objects.filter(status=1).order_by('-created_on')
        paginator = Paginator(post_list, 10)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        params = {
            'post_list': page_obj
        }

        return render(request, 'BlogApp/index.html', params)

class About(View):
    def get(self, request):
        params = dict()
        return render(request, 'BlogApp/about.html', params)

class PostDetail(DetailView):
    def get(self, request, slug):
        post = get_object_or_404(Post, slug=slug)
        params = {
            'post': post
        }
        return render(request, 'BlogApp/post_detail.html', params)