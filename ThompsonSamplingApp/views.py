from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from ThompsonSamplingApp.forms import LoadGoogleSpreadsheet
import json
import random
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')
from scipy.stats import beta
from io import BytesIO
import base64


# Create your views here.
class Index(View):

    def plot_beta(x_range, a, b, mu=0, sigma=1, cdf=False, **kwargs):
        '''
        Plots the f distribution function for a given x range, a and b
        If mu and sigma are not provided, standard beta is plotted
        If cdf=True cumulative distribution is plotted
        Passes any keyword arguments to matplotlib plot function
        '''
        x = x_range
        if cdf:
            y = beta.cdf(x, a, b, mu, sigma)
        else:
            y = beta.pdf(x, a, b, mu, sigma)
        plt.plot(x, y, **kwargs)

    def get(self, request):
        params = dict()
        form = LoadGoogleSpreadsheet()
        params['form'] = form
        return render(request, 'ThompsonSamplingApp/index.html', params)

    def post(self, request):
        form = LoadGoogleSpreadsheet(request.POST)
        if form.is_valid():
            googleSheetsId = form.cleaned_data['spreadsheetId']
            worksheetName = 'data'
            googleSheetsURL = 'https://docs.google.com/spreadsheets/d/{0}/gviz/tq?tqx=out:csv&sheet={1}'.format(
                googleSheetsId,
                worksheetName
            )
            df = pd.read_csv(googleSheetsURL)
            totalColumns = len(df.columns)
            ad = 0
            max_random = 0
            buf = BytesIO()
            x = np.linspace(0, 1, 5000)
            color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
            for i in range(1, totalColumns, 2):
                number_of_rewards_1 = df.iloc[:,i+1].sum()
                number_of_rewards_0 = df.iloc[:,i].sum()-df.iloc[:,i+1].sum()
                a, b = number_of_rewards_1 + 1, number_of_rewards_0 + 1
                random_beta = random.betavariate(a, b)
                current_ad = int((i+1)/2)
                Index.plot_beta(x, a, b, 0, 1, color=color[current_ad], lw=2, ls='-', alpha=0.5, label='Вариант '+str(current_ad))
                plt.axvline(x=random_beta, color=color[current_ad], linestyle='dotted')
                if random_beta > max_random:
                    max_random = random_beta
                    ad = current_ad
            plt.legend(loc='best', frameon=False)
            plt.savefig(buf, format='png', dpi=300)
            plt.clf()
            image_base64 = base64.b64encode(buf.getvalue()).decode('utf-8').replace('\n', '')
            buf.close()

            output = {"datatable": df.to_html(index=False), "winner": ad, "plot": image_base64}
            return HttpResponse(json.dumps(output), content_type='application/json')
        else:
            HttpResponseRedirect('/')