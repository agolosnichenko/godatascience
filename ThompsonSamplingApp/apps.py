from django.apps import AppConfig


class ThompsonsamplingappConfig(AppConfig):
    name = 'ThompsonSamplingApp'
