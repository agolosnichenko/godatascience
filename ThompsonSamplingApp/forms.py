from django import forms

class LoadGoogleSpreadsheet(forms.Form):
    spreadsheetId = forms.CharField(label='ID таблицы в Google Spreadsheet:', widget=forms.TextInput(attrs={'class':'form-control'}))