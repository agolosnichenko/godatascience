$(document).ready(function(){

    var table;

    $('#googleSpreadsheetForm').on('submit', function(e){
        e.preventDefault();
        $.post('', $(this).serialize(), function(data){
            $('#dataTable').html(data["datatable"]);
            columnsCount = $("table > tbody > tr:first > td").length;
            if (columnsCount > 0){
                appendString = '';
                metric = '';
                type = ''
                if ($('#reportTypeBanner').is(':checked')){
                    metric = 'CTR';
                    type = 'Баннер';
                } else{
                    metric = 'CR';
                    type = 'Кампания';
                }
                if (table) {
                    table.destroy();
                }
                table = $('#dataTable').DataTable({
                    paging: false,
                    ordering: false,
                    searching: false,
                    info: false,
                    scrollX: true
                });

                for (i=1; i<columnsCount; i=i+2){
                    var m = table.column(i).data().sum();
                    var n = table.column(i+1).data().sum();
                    appendString += ('<td colspan="2">'+parseFloat(n/m*100).toFixed(2)+'%</td>');
                }

                $('<tfoot><tr><td>'+metric+'</td>'+appendString+'</tr></tfoot>').appendTo($('#dataTable'));
                $('#plot').attr("src", 'data:image/png;base64,'+data["plot"]);
                $('#result').html('<b>'+type+' №'+data['winner']+' побеждает в этом раунде.</b> Нужно оставить включенным только этот вариант, остальные - нужно отключить. Спустя некоторое время нужно повторить тест, возможно победитель поменяется')
            }
            $('#section_dataTable').css('visibility', 'visible');
            $('#section_Plot').css('visibility', 'visible');
        });
    });

    $('#demo').on('click', function(){
        $('#id_spreadsheetId').val('1dJiLl6qA1YxA-Cnbx2-6vbQy8WYGAzQ5mg2UOqlrEQY');
        $('#googleSpreadsheetForm').submit();
    });
});
